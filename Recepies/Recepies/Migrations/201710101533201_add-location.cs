namespace Recepies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addlocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Lat", c => c.Double(nullable: false));
            AddColumn("dbo.Users", "Lng", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Lng");
            DropColumn("dbo.Users", "Lat");
        }
    }
}
